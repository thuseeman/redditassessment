package com.thuseeman.reddit.ui;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.thuseeman.reddit.Repository;
import com.thuseeman.reddit.model.Post;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by thuseeman on 4/23/18.
 */

@RunWith(JUnit4.class)
public class HomeVMTest {
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private HomeVM homeVM;
    private Observer<List<Post>> result;
    private Repository repository;

    @Before
    public void init() {
        homeVM = new HomeVM();
        result = mock(Observer.class);
        homeVM.getPostList().observeForever(result);
        repository = mock(Repository.class);
    }

    @Test
    public void createPost() {
        homeVM.createPost("a");
        List<Post> postList = new ArrayList<>();
        postList.add(new Post("a", 0, 0));
        verify(result).onChanged(postList);
    }

    @Test
    public void loadPosts(){
        List<Post> postList = new ArrayList<>();
        postList.add(new Post("b", 0, 0));
        when(repository.getPosts()).thenReturn(postList);
        homeVM.loadPosts(repository);
        verify(result).onChanged(postList);
    }

    @Test
    public void savePosts(){
        List<Post> postList = new ArrayList<>();
        postList.add(new Post("c", 0, 0));
        when(repository.getPosts()).thenReturn(postList);
        homeVM.loadPosts(repository);

        homeVM.savePosts(repository);
        verify(repository).savePosts(postList);
    }
}