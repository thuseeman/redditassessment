package com.thuseeman.reddit.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.thuseeman.reddit.R;
import com.thuseeman.reddit.databinding.CreateTopicDialogFragBinding;

/**
 * Created by thuseeman on 4/23/18.
 */

public class CreateTopicDialogFragment extends DialogFragment {
    private CreateTopicDialogFragBinding mBinding;
    private OnTopicCreateListener mListener;

    public static CreateTopicDialogFragment newInstance(OnTopicCreateListener listener) {
        CreateTopicDialogFragment fragment = new CreateTopicDialogFragment();
        fragment.mListener = listener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.create_topic_dialog_frag, container, false);

        mBinding.ok.setOnClickListener(v -> save());
        mBinding.cancel.setOnClickListener(v -> dismiss());
        mBinding.topic.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE){
                save();
            }
            return false;
        });

        return mBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void save() {
        String topic = mBinding.topic.getText().toString();
        if (TextUtils.isEmpty(topic)) {
            mBinding.topic.setError("Required");
            return;
        }
        mBinding.topic.setError(null);
        mListener.onTopicCreate(topic);
        dismiss();
    }



    public interface OnTopicCreateListener {
        void onTopicCreate(String topic);
    }
}
