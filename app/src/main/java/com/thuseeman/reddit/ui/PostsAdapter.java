package com.thuseeman.reddit.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.thuseeman.reddit.R;
import com.thuseeman.reddit.databinding.PostItemBinding;
import com.thuseeman.reddit.model.Post;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by thuseeman on 4/23/18.
 */

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {
    private List<Post> mPosts = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PostItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.post_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = mPosts.get(position);
        holder.binding.topic.setText(post.getTopic());
        holder.binding.upVoteCount.setText(post.getCountUpVote() + "");
        holder.binding.downVoteCount.setText(post.getCountDownVote() + "");

        holder.binding.upVote.setOnClickListener(v -> {
            post.incrementUpVoteCount();
            refresh();
        });

        holder.binding.downVote.setOnClickListener(v -> {
            post.decrementUpVoteCount();
            refresh();
        });
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        PostItemBinding binding;

        public ViewHolder(PostItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void updatePosts(List<Post> posts) {
        synchronized (mPosts) {
            Collections.sort(posts);
            mPosts.clear();
            for (Post post : posts) {
                if (mPosts.size() >= 20) break;
                mPosts.add(post);
            }
            notifyDataSetChanged();
        }
    }

    public void refresh() {
        synchronized (mPosts) {
            Collections.sort(mPosts);
            notifyDataSetChanged();
        }
    }
}
