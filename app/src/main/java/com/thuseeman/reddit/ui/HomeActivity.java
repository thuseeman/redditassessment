package com.thuseeman.reddit.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.thuseeman.reddit.R;
import com.thuseeman.reddit.Repository;
import com.thuseeman.reddit.databinding.HomeActivityBinding;

public class HomeActivity extends AppCompatActivity {
    private HomeActivityBinding mBinding;
    private HomeVM mHomeVM;
    private Repository mRepository;
    private PostsAdapter mPostsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.home_activity);
        setSupportActionBar(mBinding.toolbar);

        mHomeVM = ViewModelProviders.of(this).get(HomeVM.class);
        mRepository = new Repository(getApplicationContext());

        mBinding.posts.setHasFixedSize(true);
        mBinding.posts.setLayoutManager(new LinearLayoutManager(this));
        mPostsAdapter = new PostsAdapter();
        mBinding.posts.setAdapter(mPostsAdapter);

        mBinding.fab.setOnClickListener(v -> {
            CreateTopicDialogFragment.newInstance(topic -> {
                mHomeVM.createPost(topic);
            }).show(getSupportFragmentManager(), "create_topic");
        });

        mHomeVM.getPostList().observe(this, posts -> {
            mBinding.empty.setVisibility(posts.isEmpty() ? View.VISIBLE : View.GONE);
            mPostsAdapter.updatePosts(posts);
        });

        mHomeVM.loadPosts(mRepository);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHomeVM.savePosts(mRepository);
    }
}
