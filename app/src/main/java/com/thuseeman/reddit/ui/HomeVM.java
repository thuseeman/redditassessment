package com.thuseeman.reddit.ui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.thuseeman.reddit.Repository;
import com.thuseeman.reddit.model.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thuseeman on 4/23/18.
 */

public class HomeVM extends ViewModel {
    private MutableLiveData<List<Post>> postList;

    public LiveData<List<Post>> getPostList() {
        if (postList == null) {
            postList = new MutableLiveData<>();
        }
        return postList;
    }

    public void createPost(String topic) {
        Post post = new Post(topic, 0, 0);
        List<Post> posts = postList.getValue();
        if (posts != null) {
            posts.add(post);
        } else {
            posts = new ArrayList<>();
            posts.add(post);
        }
        postList.setValue(posts);
    }

    public void loadPosts(Repository repository) {
        List<Post> posts = repository.getPosts();
        if (posts != null) {
            postList.setValue(posts);
        }
    }

    public void savePosts(Repository repository) {
        List<Post> posts = postList.getValue();
        if (posts != null) {
            repository.savePosts(posts);
        }
    }
}
