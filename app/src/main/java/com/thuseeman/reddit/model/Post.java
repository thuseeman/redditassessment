package com.thuseeman.reddit.model;

import android.support.annotation.NonNull;

/**
 * Created by thuseeman on 4/23/18.
 */

public class Post implements Comparable<Post> {
    private String topic;
    private long countUpVote;
    private long countDownVote;

    public Post(String topic, long countUpVote, long countDownVote) {
        this.topic = topic;
        this.countUpVote = countUpVote;
        this.countDownVote = countDownVote;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public long getCountUpVote() {
        return countUpVote;
    }

    public void setCountUpVote(long countUpVote) {
        this.countUpVote = countUpVote;
    }

    public long getCountDownVote() {
        return countDownVote;
    }

    public void setCountDownVote(long countDownVote) {
        this.countDownVote = countDownVote;
    }

    @Override
    public int compareTo(@NonNull Post post) {
        return new Long(post.getCountUpVote()).compareTo(getCountUpVote());
    }

    public void incrementUpVoteCount() {
        countUpVote++;
    }

    public void decrementUpVoteCount() {
        countDownVote++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;

        Post post = (Post) o;

        if (countUpVote != post.countUpVote) return false;
        if (countDownVote != post.countDownVote) return false;
        return topic != null ? topic.equals(post.topic) : post.topic == null;
    }

    @Override
    public int hashCode() {
        int result = topic != null ? topic.hashCode() : 0;
        result = 31 * result + (int) (countUpVote ^ (countUpVote >>> 32));
        result = 31 * result + (int) (countDownVote ^ (countDownVote >>> 32));
        return result;
    }
}
