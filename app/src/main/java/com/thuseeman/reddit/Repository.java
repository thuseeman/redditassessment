package com.thuseeman.reddit;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thuseeman.reddit.model.Post;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thuseeman on 4/23/18.
 */

public class Repository {
        private static final String POSTS = "posts";
        private SharedPreferences mSp;

        public Repository(Context context) {
            if (mSp == null) {
                mSp = PreferenceManager.getDefaultSharedPreferences(context);
            }
        }

        public List<Post> getPosts() {
            String postsJson = mSp.getString(POSTS, null);
            List<Post> posts = new ArrayList<>();
            if (!TextUtils.isEmpty(postsJson)) {
                Type typeToken = new TypeToken<List<Post>>() {
                }.getType();
                return new Gson().fromJson(postsJson, typeToken);
            }
            return posts;
        }

        public void savePosts(List<Post> posts) {
            mSp.edit().putString(POSTS, new Gson().toJson(posts)).apply();
        }
}
