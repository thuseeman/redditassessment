package com.thuseeman.reddit;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by thuseeman on 4/23/18.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
